<b>Xeonjia</b> es un <b>juego de aventura</b> que toma lugar en un <b>mundo congelado</b>.

¡El suelo está congelado! Piensa con atención sobre tus movimientos porque no puedes cambiar la dirección hasta que te encuentres con un obstáculo.
¡Usa tu intelecto para resolver los rompecabezas!

El mundo fue congelado por el <b>«Rey del Mal»</b> y ya no es un lugar seguro.

La leyenda dice que una persona heroica y valiente derrotará al Rey del Mal y salvará el reino… Por este motivo cada año una persona es escogida como el «héroe» al que se le encomienda la aventura de salvar el reino.

Ahora es tu momento, <b>¡te han escogido como el héroe de este año!</b> ¿Podrás derrotar al Rey del Mal?

Vas a tener que viajar alrededor del mundo, explorar ciudades, calabozos y místicas cuevas.

<b>¿Quieres resolver las misiones y los rompecabezas de este mundo RPG congelado?</b>

Con el tiempo ganarás puntos de experiencia (XP) que te permitirán incrementar tu nivel, así como encontrarás dinero y tesoros ocultos que puedes usar para comprar cosas o mejorarte.

Ten cuidado, ¡el mundo está lleno de <b>peligrosos enemigos</b> listos para atacarte!

Ten en cuenta, en gran medida <b>el piso está congelado</b>, así que no puedes detenerte hasta que des con un muro, una roca o cualquier otro tipo de obstáculo.

<b>¡Usa tu intelecto para encontrar el mejor camino!</b>
