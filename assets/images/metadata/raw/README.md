The files in this directory (assets/images/metadata/raw/) are NOT bundled and deployed in the app.

These files are created using the Fire Atlas Editor.

Command `flutter pub run update_xfa_files.dart` is used to remove the 'imageData' property and create .xfa files in assets/images/metadata.
