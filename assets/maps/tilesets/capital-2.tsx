<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.0" name="capital-2" tilewidth="16" tileheight="16" tilecount="168" columns="21">
 <image source="../../images/capital-2.png" width="336" height="128"/>
 <tile id="0" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="2" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="3" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="4" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="5" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="6" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="7" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="8" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="9" class="Ground">
  <properties>
   <property name="flying" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="10" class="Solid"/>
 <tile id="11" class="Solid"/>
 <tile id="12" class="Solid"/>
 <tile id="13" class="Solid"/>
 <tile id="14" class="Solid"/>
 <tile id="19" class="Solid"/>
 <tile id="20" class="Solid"/>
 <tile id="21" class="Solid"/>
 <tile id="22" class="Solid"/>
 <tile id="23" class="Solid"/>
 <tile id="24" class="Solid"/>
 <tile id="25" class="Solid"/>
 <tile id="26" class="Solid"/>
 <tile id="27" class="Solid"/>
 <tile id="28" class="Solid"/>
 <tile id="29" class="Solid"/>
 <tile id="30" class="Solid"/>
 <tile id="31" class="Solid"/>
 <tile id="32" class="Solid"/>
 <tile id="33" class="Solid"/>
 <tile id="34" class="Solid"/>
 <tile id="35" class="Solid"/>
 <tile id="36" class="Solid"/>
 <tile id="37" class="Solid"/>
 <tile id="38" class="Solid"/>
 <tile id="39" class="Solid"/>
 <tile id="40" class="Solid"/>
 <tile id="41" class="Solid"/>
 <tile id="42" class="Solid"/>
 <tile id="43" class="Solid"/>
 <tile id="44" class="Solid"/>
 <tile id="45" class="Ground"/>
 <tile id="46" class="Ground"/>
 <tile id="47" class="Ground"/>
 <tile id="48" class="Ground"/>
 <tile id="49" class="Solid"/>
 <tile id="50" class="Solid"/>
 <tile id="51" class="Solid"/>
 <tile id="52" class="Solid"/>
 <tile id="53" class="Solid"/>
 <tile id="54" class="Solid"/>
 <tile id="55" class="Solid"/>
 <tile id="56" class="Solid"/>
 <tile id="57" class="Solid"/>
 <tile id="58" class="Solid"/>
 <tile id="59" class="Solid"/>
 <tile id="60" class="Solid"/>
 <tile id="61" class="Solid"/>
 <tile id="62" class="Solid"/>
 <tile id="63" class="Solid"/>
 <tile id="64" class="Solid"/>
 <tile id="65" class="Solid"/>
 <tile id="66" class="Ground"/>
 <tile id="67" class="Ground"/>
 <tile id="68" class="Ground"/>
 <tile id="69" class="Ground"/>
 <tile id="71" class="Solid"/>
 <tile id="72" class="Solid"/>
 <tile id="73" class="Solid"/>
 <tile id="75" class="Solid"/>
 <tile id="76" class="Solid"/>
 <tile id="77" class="Solid"/>
 <tile id="79" class="Solid"/>
 <tile id="80" class="Solid"/>
 <tile id="81" class="Solid"/>
 <tile id="82" class="Solid"/>
 <tile id="83" class="Solid"/>
 <tile id="84" class="Solid"/>
 <tile id="85" class="Solid"/>
 <tile id="86" class="Solid"/>
 <tile id="87" class="Ground"/>
 <tile id="88" class="Ground"/>
 <tile id="89" class="Ground"/>
 <tile id="90" class="Ground"/>
 <tile id="91" class="Solid"/>
 <tile id="93" class="Solid"/>
 <tile id="94" class="Solid"/>
 <tile id="95" class="Solid"/>
 <tile id="96" class="Solid"/>
 <tile id="97" class="Solid"/>
 <tile id="98" class="Solid"/>
 <tile id="104" class="Ground"/>
 <tile id="105" class="Solid"/>
 <tile id="106" class="Solid"/>
 <tile id="107" class="Solid"/>
 <tile id="108" class="Solid"/>
 <tile id="109" class="Solid"/>
 <tile id="110" class="Solid"/>
 <tile id="111" class="Solid"/>
 <tile id="112" class="Solid"/>
 <tile id="113" class="Solid"/>
 <tile id="114" class="Solid"/>
 <tile id="115" class="Solid"/>
 <tile id="118" class="Solid"/>
 <tile id="119" class="Solid"/>
 <tile id="121" class="Solid"/>
 <tile id="122" class="Solid"/>
 <tile id="123" class="Solid"/>
 <tile id="124" class="Solid"/>
 <tile id="125" class="Solid"/>
 <tile id="126" class="Solid"/>
 <tile id="127" class="Solid"/>
 <tile id="128" class="Solid"/>
 <tile id="129" class="Solid"/>
 <tile id="130" class="Solid"/>
 <tile id="131" class="Solid"/>
 <tile id="132" class="Solid"/>
 <tile id="133" class="Solid"/>
 <tile id="134" class="Solid"/>
 <tile id="135" class="Solid"/>
 <tile id="136" class="Solid"/>
 <tile id="137" class="Solid"/>
 <tile id="138" class="Solid"/>
 <tile id="139" class="Solid"/>
 <tile id="140" class="Solid"/>
 <tile id="141" class="Solid"/>
 <tile id="142" class="Solid"/>
 <tile id="143" class="Ground"/>
 <tile id="144" class="Solid"/>
 <tile id="145" class="Ground"/>
 <tile id="146" class="ThinWall">
  <properties>
   <property name="priority" type="int" value="500"/>
   <property name="solidSide" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="147" class="Solid"/>
 <tile id="148" class="Solid"/>
 <tile id="149" class="Solid"/>
 <tile id="150" class="Solid"/>
 <tile id="151" class="Solid"/>
 <tile id="153" class="Solid"/>
 <tile id="154" class="Solid"/>
 <tile id="155" class="Solid"/>
 <tile id="157" class="Solid"/>
 <tile id="158" class="Solid"/>
 <tile id="159" class="Solid"/>
 <tile id="160" class="Solid"/>
 <tile id="161" class="Solid"/>
 <tile id="162" class="Solid"/>
 <tile id="163" class="Solid"/>
 <tile id="164" class="Solid"/>
 <tile id="165" class="Solid"/>
 <tile id="166" class="Ground"/>
 <tile id="167" class="ThinWall">
  <properties>
   <property name="priority" type="int" value="500"/>
   <property name="solidSide" type="int" value="2"/>
  </properties>
 </tile>
</tileset>
