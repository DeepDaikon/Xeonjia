<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.0" name="npc_common" tilewidth="16" tileheight="16" tilecount="52" columns="4">
 <image source="../../images/npc_common.png" width="64" height="208"/>
 <tile id="0" class="NPC">
  <properties>
   <property name="name" value="man"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="1" class="NPC">
  <properties>
   <property name="name" value="man"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="2" class="NPC">
  <properties>
   <property name="name" value="man"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="3" class="NPC">
  <properties>
   <property name="name" value="man"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="4" class="NPC">
  <properties>
   <property name="name" value="lumberjack"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="5" class="NPC">
  <properties>
   <property name="name" value="lumberjack"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="6" class="NPC">
  <properties>
   <property name="name" value="lumberjack"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="7" class="NPC">
  <properties>
   <property name="name" value="lumberjack"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="8" class="NPC">
  <properties>
   <property name="name" value="young-girl"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="9" class="NPC">
  <properties>
   <property name="name" value="young-girl"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="10" class="NPC">
  <properties>
   <property name="name" value="young-girl"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="11" class="NPC">
  <properties>
   <property name="name" value="young-girl"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="12" class="NPC">
  <properties>
   <property name="name" value="youngster"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="13" class="NPC">
  <properties>
   <property name="name" value="youngster"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="14" class="NPC">
  <properties>
   <property name="name" value="youngster"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="15" class="NPC">
  <properties>
   <property name="name" value="youngster"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="16" class="NPC">
  <properties>
   <property name="name" value="elderly"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="17" class="NPC">
  <properties>
   <property name="name" value="elderly"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="18" class="NPC">
  <properties>
   <property name="name" value="elderly"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="19" class="NPC">
  <properties>
   <property name="name" value="elderly"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="20" class="NPC">
  <properties>
   <property name="name" value="guy"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="21" class="NPC">
  <properties>
   <property name="name" value="guy"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="22" class="NPC">
  <properties>
   <property name="name" value="guy"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="23" class="NPC">
  <properties>
   <property name="name" value="guy"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="24" class="NPC">
  <properties>
   <property name="name" value="old-man"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="25" class="NPC">
  <properties>
   <property name="name" value="old-man"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="26" class="NPC">
  <properties>
   <property name="name" value="old-man"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="27" class="NPC">
  <properties>
   <property name="name" value="old-man"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="28" class="NPC">
  <properties>
   <property name="name" value="adventurer"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="29" class="NPC">
  <properties>
   <property name="name" value="adventurer"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="30" class="NPC">
  <properties>
   <property name="name" value="adventurer"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="31" class="NPC">
  <properties>
   <property name="name" value="adventurer"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="32" class="NPC">
  <properties>
   <property name="name" value="dude"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="33" class="NPC">
  <properties>
   <property name="name" value="dude"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="34" class="NPC">
  <properties>
   <property name="name" value="dude"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="35" class="NPC">
  <properties>
   <property name="name" value="dude"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="36" class="NPC">
  <properties>
   <property name="name" value="girl"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="37" class="NPC">
  <properties>
   <property name="name" value="girl"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="38" class="NPC">
  <properties>
   <property name="name" value="girl"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="39" class="NPC">
  <properties>
   <property name="name" value="girl"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="40" class="NPC">
  <properties>
   <property name="name" value="oldster"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="41" class="NPC">
  <properties>
   <property name="name" value="oldster"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="42" class="NPC">
  <properties>
   <property name="name" value="oldster"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="43" class="NPC">
  <properties>
   <property name="name" value="oldster"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="44" class="NPC">
  <properties>
   <property name="name" value="henchman"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="45" class="NPC">
  <properties>
   <property name="name" value="henchman"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="46" class="NPC">
  <properties>
   <property name="name" value="henchman"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="47" class="NPC">
  <properties>
   <property name="name" value="henchman"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="48" class="NPC">
  <properties>
   <property name="name" value="worker"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="49" class="NPC">
  <properties>
   <property name="name" value="worker"/>
   <property name="orientation" type="int" value="1"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="50" class="NPC">
  <properties>
   <property name="name" value="worker"/>
   <property name="orientation" type="int" value="2"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="51" class="NPC">
  <properties>
   <property name="name" value="worker"/>
   <property name="orientation" type="int" value="3"/>
   <property name="team" type="int" value="2"/>
  </properties>
 </tile>
</tileset>
