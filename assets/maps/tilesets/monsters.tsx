<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.0" name="monsters" tilewidth="16" tileheight="16" tilecount="75" columns="5">
 <image source="../../images/monsters.png" width="80" height="240"/>
 <tile id="0" class="Monster">
  <properties>
   <property name="action" value="(dialog '((&quot;monster/&quot; &quot;Gyaaa!&quot;)))"/>
   <property name="name" value="green"/>
  </properties>
 </tile>
 <tile id="5" class="Monster">
  <properties>
   <property name="name" value="green"/>
   <property name="orientation" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="10" class="Monster">
  <properties>
   <property name="name" value="green"/>
   <property name="orientation" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="15" class="Monster">
  <properties>
   <property name="name" value="green"/>
   <property name="orientation" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="25" class="Monster">
  <properties>
   <property name="action" value="(dialog '((&quot;monster/&quot; &quot;Gyaaa!&quot;)))"/>
   <property name="name" value="blue"/>
  </properties>
 </tile>
 <tile id="30" class="Monster">
  <properties>
   <property name="name" value="blue"/>
  </properties>
 </tile>
 <tile id="35" class="Monster">
  <properties>
   <property name="name" value="blue"/>
  </properties>
 </tile>
 <tile id="40" class="Monster">
  <properties>
   <property name="name" value="blue"/>
  </properties>
 </tile>
 <tile id="50" class="Monster">
  <properties>
   <property name="action" value="(dialog '((&quot;monster/&quot; &quot;Gyaaa!&quot;)))"/>
   <property name="name" value="red"/>
  </properties>
 </tile>
</tileset>
