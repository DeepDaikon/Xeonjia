import 'package:flutter/material.dart';
import 'package:xeonjia/game/xeonjia_game.dart';

/// Button used to open the backpack
class BackpackButton extends StatelessWidget {
  BackpackButton(this.gameRef);
  final XeonjiaGame gameRef;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 6,
      right: gameRef.map.disableMiniMap ? 6 : 50,
      child: InkWell(
        onTap: () => gameRef.isBackpackButtonActive ? gameRef.backpack() : null,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
          height: 34,
          decoration: BoxDecoration(
              color: Colors.grey.shade800.withOpacity(0.7),
              borderRadius: const BorderRadius.all(Radius.circular(30))),
          child: const Icon(Icons.backpack, color: Colors.white),
        ),
      ),
    );
  }
}
